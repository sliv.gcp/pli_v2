#!/usr/bin/env node
import 'reflect-metadata';
import { Container } from 'inversify';
import { PliEnvProps } from './props/env-props';
import { ExecutionService } from './service/execution-service';


(async function main() {
    const container = new Container();
    container.bind(PliEnvProps).toSelf().inSingletonScope();
    container.bind(ExecutionService).toSelf().inSingletonScope();
    const execService = container.get(ExecutionService);
    execService.run();
})().catch(console.error);