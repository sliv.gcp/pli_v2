import { injectable } from 'inversify';
import { sh } from 'cmd-template';
import { pliTree } from '../builtin/pli-tree';

@injectable()
export class PliEnvProps {
    argv = process.argv;
    tree = {
        async '@init'() {
            return {
                sh: sh.bind(sh),
                hello: 'world',
                clear: () => sh`clear`,
            };
        },
        ...pliTree,
        stream: {
            '@init'() {
                return { color: 'red' };
            },
            amaz: [
                `clear`,
                `date`,
                `ls -l`,
                `date`,
            ],
        },
    };
}