export function rawStringToTemplateStringArr(str: string) {
    const raw = [str];
    return Object.assign(raw, { raw });
}