export const isType = t => s => typeof s === t;
export const isFunc = isType('function');
export const isArr = Array.isArray;
export const isStr = isType('string');