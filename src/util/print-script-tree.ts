import * as treeify from 'treeify';
import { toPairs, reduce, startsWith, is } from 'ramda';
import { matchHarness } from 'ramda-match';
import { pliTree } from '../builtin/pli-tree';
import { green } from 'chalk';

const maxCmdTextLength = 50;

function trimText(text: string) {
    if (text.length >= maxCmdTextLength) {
        return text.slice(0, maxCmdTextLength - 3) + ' ...';
    }
    return text;
}

export function normalizePrintTree(obj) {
    return toPairs(obj).reduce((pr, [k, v]: [string, any]) => {
        return matchHarness(m => {
            m(() => startsWith('@', k), () => pr);
            m(is(String), () => {
                const key = `${k}\t${green(trimText(v.slice(0, 50)))}`;
                pr[key] = true;
                return pr;
            });
            m(Array.isArray, () => {
                const shortCmd = trimText(v.join(';').slice(0, 50));
                const key = `${k}\t${green(shortCmd)}`;
                pr[key] = true;
                return pr;
            });
            m(is(Object), () => {
                pr[k] = normalizePrintTree(v);
                return pr;
            });
        })(v);
    }, {});
}

export function printScriptTree(tree) {
    console.log(treeify.asTree(normalizePrintTree(tree)));
}

printScriptTree(pliTree);