#!/usr/bin/env node
import 'reflect-metadata';
import { Container } from 'inversify';
import { PliEnvProps } from './props/env-props';
import { CompletionService } from './service/completion-service';

(async function main() {
    const container = new Container();
    container.bind(PliEnvProps).toSelf().inSingletonScope();
    container.bind(CompletionService).toSelf().inSingletonScope();
    container.get(CompletionService).complete();
})().catch(console.error);