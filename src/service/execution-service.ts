import { injectable } from 'inversify';
import { sh } from 'cmd-template';
import { rawStringToTemplateStringArr } from '../util/str';
import { isArr, isFunc, isStr } from '../util/is';
import { matchHarness } from 'ramda-match';
import { path } from 'ramda';
import { PliEnvProps } from '../props/env-props';
import { isNullOrUndefined } from 'util';
import parseArgv = require('minimist');

@injectable()
export class ExecutionService {
    constructor(
        private envs: PliEnvProps,
    ) {
    }

    async run() {
        const { argv, tree } = this.envs;
        const { _, ...restOpt } = parseArgv(argv.slice(2));
        const [treePath = 'stream.amaz', ...restArg] = _;
        const pathArr = treePath.split('.');
        const target: any = path(pathArr, tree);
        const defaultToolkit = {
            sh: sh.bind(sh),
            pliEnvProps: this.envs,
        };
        let toolkit = {
            ...defaultToolkit
        };
        let node = tree;

        // initialize the toolkit
        for (const k of pathArr) {
            const init = node['@init'];
            if (init) toolkit = { ...toolkit, ...(await init() || {}) };
            node = node[k];
        }

        async function runArr(cmds) {
            for (const cmd of cmds) {
                const cmdTemplateArr = rawStringToTemplateStringArr(cmd);
                await sh(cmdTemplateArr);
            }
        }

        matchHarness(m => {
            m(isFunc, (s) => s({ _: restArg, ...restOpt }, toolkit));
            m(isArr, runArr);
            m(isStr, () => runArr([target]));
            m(isNullOrUndefined, () => {
                console.error('not found');
            });
        })(target);
    }
}
