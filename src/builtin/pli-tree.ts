import { homedir } from 'os';
import { resolve } from 'path';
import { PliEnvProps } from '../props/env-props';
import { writeFile } from 'fs';
import * as treeify from 'treeify';
import { commonCommandTree } from './user';
import { printScriptTree } from '../util/print-script-tree';

export const pliTree = {
    pli: {
        '@init'() {
            const { writeFile, readFile } = require('fs');
            return {
                writeFile,
                readFile,
            };
        },
        edit: [
            `$EDITOR ${resolve(homedir(), '.pli', 'items.json')}`,
        ],
        async add({ _ }, { writeFile }) {
            const path = resolve(homedir(), '.pli', 'items.json');
            await new Promise(r => writeFile(path, JSON.stringify(_.map(s => resolve(process.cwd(), s))), r));
        },
        async remove() {

        },
        async open({}, { sh }) {
            await sh`open ${resolve(homedir(), '.pli')}`;
        },
        print({}, { readFile, pliEnvProps }) {
            printScriptTree(pliEnvProps.tree);
        },
    },
    ...commonCommandTree
};